package com.insarennes.rnsapp.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.model.LineUpItem;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class LineUpFragment extends Fragment implements MaterialTabListener {

    public static final String INITIAL_DAY_KEY = "DayID";

    protected MaterialTabHost tabHost;
    protected ViewPager pager;
    protected ViewPagerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int initialDay = getArguments().getInt(INITIAL_DAY_KEY);

        View view = inflater.inflate(R.layout.fragment_line_up, container, false);

        tabHost = (MaterialTabHost) view.findViewById(R.id.tab_host);
        pager = (ViewPager) view.findViewById(R.id.view_pager);

        // init view pager
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);
            }
        });

        // insert all tabs from pagerAdapter data
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(tabHost.newTab()
                            .setText(adapter.getPageTitle(i))
                            .setTabListener(this)
            );
        }

        if (initialDay >= 0 && initialDay < adapter.getCount()) {
            pager.setCurrentItem(initialDay);
        }

        return view;
    }

    @Override
    public void onTabSelected(MaterialTab tab) {
        pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {
    }

    @Override
    public void onTabUnselected(MaterialTab tab) {
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private PageDetails[] pages = new PageDetails[]{
                new PageDetails(getString(R.string.thursday), 14),
                new PageDetails(getString(R.string.friday), 15),
                new PageDetails(getString(R.string.saturday), 16),
        };

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int position) {
            return LineUpDayFragment.newInstance(pages[position].day);
        }

        @Override
        public int getCount() {
            return pages.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pages[position].title;
        }

    }

    class PageDetails {
        String title;
        int day;

        PageDetails(String title, int day) {
            this.title = title;
            this.day = day;
        }
    }
}
