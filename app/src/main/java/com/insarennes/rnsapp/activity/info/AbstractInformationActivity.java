package com.insarennes.rnsapp.activity.info;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.insarennes.rnsapp.R;

public abstract class AbstractInformationActivity extends ActionBarActivity {

    protected LinearLayout mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLayout = (LinearLayout) findViewById(R.id.infos_layout);
    }

    protected void addText(int textId) {
        TextView festival = new TextView(this);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        llp.setMargins(10, 10, 10, 10);
        festival.setLayoutParams(llp);
        festival.setTextAppearance(getApplicationContext(), R.style.BandActivityTextStyle);
        festival.setText(getText(textId));
        festival.setMovementMethod(LinkMovementMethod.getInstance());
        mLayout.addView(festival);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
        }
        return true;
    }
}
