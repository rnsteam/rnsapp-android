package com.insarennes.rnsapp.activity.info;

import android.os.Bundle;

import com.insarennes.rnsapp.R;

public class PraticalInformationActivity extends AbstractInformationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getResources().getString(R.string.infos));

        addText(R.string.infos1);
        addText(R.string.infos2);
    }
}
