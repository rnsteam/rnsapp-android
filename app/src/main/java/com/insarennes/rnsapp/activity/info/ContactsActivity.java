package com.insarennes.rnsapp.activity.info;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;
import com.insarennes.rnsapp.model.Link;
import com.insarennes.rnsapp.view.LinkListViewHelper;

public class ContactsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_contacts);

        setTitle(getResources().getString(R.string.contacts));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView emailsTitle = (TextView) findViewById(R.id.contacts_email_title);
        TextView forumsTitle = (TextView) findViewById(R.id.contacts_forum_title);
        TextView linksTitle = (TextView) findViewById(R.id.contacts_links_title);
        LinearLayout emails = (LinearLayout) findViewById(R.id.contacts_email_links);
        LinearLayout forums = (LinearLayout) findViewById(R.id.contacts_forum_links);
        LinearLayout links = (LinearLayout) findViewById(R.id.contacts_links);

        Typeface arcaHeavyFont = CustomFontsLoader.getTypeface(this, CustomFontsLoader.ARCA_HEAVY);
        emailsTitle.setTypeface(arcaHeavyFont);
        forumsTitle.setTypeface(arcaHeavyFont);
        linksTitle.setTypeface(arcaHeavyFont);

        LayoutInflater inflater = LayoutInflater.from(this);
        LinkListViewHelper linkListViewHelper = new LinkListViewHelper(getResources());
        linkListViewHelper.addLink(inflater, emails, new Link("info@rocknsolex.fr"));
        linkListViewHelper.addLink(inflater, forums, new Link("forum.rocknsolex.fr"));
        linkListViewHelper.addLink(inflater, links, new Link("www.rocknsolex.fr"));
        linkListViewHelper.addLink(inflater, links, new Link("www.facebook.com/rocknsolex"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
        }
        return true;
    }
}
