package com.insarennes.rnsapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.model.Race;
import com.insarennes.rnsapp.model.RnsModel;

public class RaceFragment extends Fragment {

    private static final String ARG_RACE = "race";
    private int mRace;

    public static RaceFragment newInstance(int raceId) {
        RaceFragment fragment = new RaceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_RACE, raceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRace = getArguments().getInt(ARG_RACE);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_race, container, false);

        final Race race = RnsModel.getInstance().getRaces().getRaceFromId(mRace);

        ImageButton mapButton = (ImageButton) view.findViewById(R.id.races_map_button);
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ImageViewerActivity.class);
                i.putExtra(ImageViewerActivity.EXTRA_IMAGE, race.getMapResId());
                i.putExtra(ImageViewerActivity.EXTRA_TITLE, race.getName());
                startActivity(i);
            }
        });

        if(race.getMapResId() == 0) {
            mapButton.setVisibility(View.GONE);
        }

        TextView infosTextView = (TextView) view.findViewById(R.id.races_infos_textview);
        infosTextView.setText(race.getInfos());


        return view;
    }
}