package com.insarennes.rnsapp.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.insarennes.rnsapp.R;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

/**
 * Display an image with pan and zoom features.
 * EXTRA_IMAGE is a resource id (R.drawable.xxx)
 */
public class ImageViewerActivity extends ActionBarActivity {

    public final static String EXTRA_IMAGE = "com.insarennes.rnsapp.activity.ImageViewerActivity.EXTRA_IMAGE";
    public final static String EXTRA_TITLE = "com.insarennes.rnsapp.activity.ImageViewerActivity.EXTRA_TITLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_viewer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        if (intent.hasExtra(EXTRA_IMAGE)) {
            int imageResId = intent.getIntExtra(EXTRA_IMAGE, R.drawable.rns_logo_2015);
            ImageViewTouch image = (ImageViewTouch) findViewById(R.id.image);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imageResId);
            image.setImageBitmap(bitmap, null, -1, 8f);
            // Remark : if out of memory probleme => reduce size of bitmap received here
        }

        if (intent.hasExtra(EXTRA_TITLE)) {
            String title = intent.getStringExtra(EXTRA_TITLE);
            setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
        }
        return true;
    }
}
