package com.insarennes.rnsapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.common.collect.Iterables;
import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.adapter.DrawerAdapter;
import com.insarennes.rnsapp.model.Band;
import com.insarennes.rnsapp.model.LineUpItem;
import com.insarennes.rnsapp.model.RnsModel;

import java.util.Calendar;

public class MainActivity extends ActionBarActivity {

    private ListView mDrawerList;
    private DrawerAdapter mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawLineaLayout;
    private Fragment mCurrentFragment;

    private String[] mSections;
    private int mCurrentSelectedPosition = 0;

    protected int mLineUpDayToShow = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawLineaLayout = (LinearLayout) findViewById(R.id.drawer_linearlayout);

        mSections = getResources().getStringArray(R.array.sections);

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // TODO gérer saveinstance stat pour la current selection position
        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mLineUpDayToShow != -1) {
            showLineUp();
        }
    }

    private void addDrawerItems() {
        mAdapter = new DrawerAdapter(
                getBaseContext(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                mSections);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getSupportActionBar().setTitle(...);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mCurrentFragment.onCreateOptionsMenu(menu, getMenuInflater());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (mCurrentFragment.onOptionsItemSelected(item)) {
            return true;
        }

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            // If open line up request, set mLineUpDayToShow to show the lineup onResume
            if (requestCode == BandActivity.OPEN_LINEUP_REQUESTCODE) {
                int bandId = data.getExtras().getInt(BandActivity.BAND_ID_KEY);

                RnsModel model = RnsModel.getInstance();
                Band band = model.getBands().getBandFromId(bandId);
                LineUpItem lineUp = Iterables.getFirst(model.getLineUp().getLineUpItems(band), null);

                if (lineUp != null) {
                    mLineUpDayToShow = lineUp.getDay().get(Calendar.DAY_OF_MONTH) - 14;
                }
            }
        }
    }

    public void showLineUp() {
        selectItem(2);
        mLineUpDayToShow = -1;
    }

    public void showLineUp(int day) {
        mLineUpDayToShow = day;
        selectItem(2);
        mLineUpDayToShow = -1;
    }

    public void showRaces() {
        selectItem(3);
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        mCurrentFragment = createFragment(position);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.drawer_container, mCurrentFragment)
                .commit();

        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mSections[position]);
        mDrawerLayout.closeDrawer(mDrawLineaLayout);
    }

    private Fragment createFragment(int position) {
        Fragment fragment;

        switch (position) {

            case 0:
                fragment = new HomeFragment();
                break;

            case 1:
                fragment = new BandsFragment();
                break;

            case 2:
                fragment = new LineUpFragment();
                Bundle args = new Bundle();
                if (mLineUpDayToShow != -1) {
                    args.putInt(LineUpFragment.INITIAL_DAY_KEY, mLineUpDayToShow);
                }
                fragment.setArguments(args);
                break;

            case 3:
                fragment = new RacesFragment();
                break;

            case 4:
                fragment = new InformationFragment();
                break;

            default:
                fragment = new BlankFragment();
                break;

        }
        return fragment;
    }
}
