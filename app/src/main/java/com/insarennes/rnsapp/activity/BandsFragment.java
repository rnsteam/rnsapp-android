package com.insarennes.rnsapp.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.adapter.BandsAdapter;
import com.insarennes.rnsapp.model.RnsModel;
import com.insarennes.rnsapp.view.AutoFitRecyclerView;

public class BandsFragment extends Fragment {

    public static final int NB_COLUMN_GRID = 2;

    protected BandsAdapter mAdapter;

    public BandsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bands, container, false);

        AutoFitRecyclerView mRecyclerView = (AutoFitRecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new BandsAdapter(this.getActivity(), RnsModel.getInstance().getBands(), RnsModel.getInstance().getLineUp());
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_bands, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_sort_by_name:
                mAdapter.setSort(BandsAdapter.BandSort.NAME);
                return true;
            case R.id.action_sort_by_date:
                mAdapter.setSort(BandsAdapter.BandSort.DATE);
                return true;
            case R.id.action_sort_by_headlinings:
                mAdapter.setSort(BandsAdapter.BandSort.HEADLININGS);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
