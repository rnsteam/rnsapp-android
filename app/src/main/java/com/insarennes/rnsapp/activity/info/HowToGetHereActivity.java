package com.insarennes.rnsapp.activity.info;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;

public class HowToGetHereActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_howtogethere);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.howToGetHere));

        TextView trainTitle = (TextView) findViewById(R.id.howtogethere_train_title);
        TextView busTitle = (TextView) findViewById(R.id.howtogethere_bus_title);
        TextView carTitle = (TextView) findViewById(R.id.howtogethere_car_title);

        Typeface arcaHeavyFont = CustomFontsLoader.getTypeface(this, CustomFontsLoader.ARCA_HEAVY);
        trainTitle.setTypeface(arcaHeavyFont);
        busTitle.setTypeface(arcaHeavyFont);
        carTitle.setTypeface(arcaHeavyFont);
    }

    public void startNavigation(View view) {
        double latitude = 48.120804;
        double longitude = -1.63575;
        String label = "INSA Rennes";
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = latitude + "," + longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
        }
        return true;
    }
}
