package com.insarennes.rnsapp.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.model.Race;
import com.insarennes.rnsapp.model.Races;
import com.insarennes.rnsapp.model.RnsModel;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class RacesFragment extends Fragment implements MaterialTabListener {

    protected MaterialTabHost tabHost;
    protected ViewPager pager;
    protected ViewPagerAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_races, container, false);

        tabHost = (MaterialTabHost) view.findViewById(R.id.tab_host);
        pager = (ViewPager) view.findViewById(R.id.view_pager);

        // init view pager
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);
            }
        });

        // insert all tabs from pagerAdapter data
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(tabHost.newTab()
                            .setText(adapter.getPageTitle(i))
                            .setTabListener(this)
            );
        }

        return view;
    }

    @Override
    public void onTabSelected(MaterialTab tab) {
        pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {
    }

    @Override
    public void onTabUnselected(MaterialTab tab) {
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private PageDetails[] pages;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            Races races = RnsModel.getInstance().getRaces();
            pages = new PageDetails[races.getRaces().size()];
            int i = 0;
            for (Race race : RnsModel.getInstance().getRaces().getRaces()) {
                pages[i++] = new PageDetails(race.getName(), race.getId());
            }
        }

        public Fragment getItem(int position) {
            return RaceFragment.newInstance(pages[position].raceId);
        }

        @Override
        public int getCount() {
            return pages.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pages[position].title;
        }

    }

    class PageDetails {
        String title;
        int raceId;

        PageDetails(String title, int raceId) {
            this.title = title;
            this.raceId = raceId;
        }
    }
}
