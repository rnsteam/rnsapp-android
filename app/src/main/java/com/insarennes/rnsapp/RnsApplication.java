package com.insarennes.rnsapp;

import android.app.Application;

import com.insarennes.rnsapp.loader.RnsModelLoader;
import com.insarennes.rnsapp.model.RnsModel;

public class RnsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Load data :
        RnsModelLoader.loadData(RnsModel.getInstance(), getResources());
    }
}
