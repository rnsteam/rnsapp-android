package com.insarennes.rnsapp.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.model.RnsModel;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class CountDownView extends FrameLayout {

    protected Calendar mDeadLine;

    protected Thread mCountDownThread;
    protected Boolean mIsCounting;

    protected TextView mDay1;
    protected TextView mDay2;
    protected TextView mHour1;
    protected TextView mHour2;
    protected TextView mMinute1;
    protected TextView mMinute2;
    protected TextView mSecond1;
    protected TextView mSecond2;

    public CountDownView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CountDownView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CountDownView(Context context) {
        super(context);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_countdown, null);
        addView(view);

        mDay1 = (TextView) view.findViewById(R.id.countdown_day_1);
        mDay2 = (TextView) view.findViewById(R.id.countdown_day_2);
        mHour1 = (TextView) view.findViewById(R.id.countdown_hour_1);
        mHour2 = (TextView) view.findViewById(R.id.countdown_hour_2);
        mMinute1 = (TextView) view.findViewById(R.id.countdown_minute_1);
        mMinute2 = (TextView) view.findViewById(R.id.countdown_minute_2);
        mSecond1 = (TextView) view.findViewById(R.id.countdown_second_1);
        mSecond2 = (TextView) view.findViewById(R.id.countdown_second_2);
    }

    public void setDeadLine(Calendar deadLine) {
        this.mDeadLine = deadLine;
    }

    public void startCountdown()
    {
        mIsCounting = true;
        Runnable runnable = new CountDownRunner();
        mCountDownThread = new Thread(runnable);
        mCountDownThread.start();
    }

    public  void stopCountdown()
    {
        mIsCounting = false;
    }

    public void doCountDown() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                try {
                    Calendar now = Calendar.getInstance();

                    long diff = mDeadLine.getTime().getTime() - now.getTime().getTime();

                    long days = TimeUnit.MILLISECONDS.toDays(diff);
                    diff -= TimeUnit.DAYS.toMillis(days);
                    long hours = TimeUnit.MILLISECONDS.toHours(diff);
                    diff -= TimeUnit.HOURS.toMillis(hours);
                    long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
                    diff -= TimeUnit.MINUTES.toMillis(minutes);
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);

                    String day1 = Long.toString(days / 10);
                    String day2 = Long.toString(days % 10);
                    String hour1 = Long.toString(hours / 10);
                    String hour2 = Long.toString(hours % 10);
                    String minute1 = Long.toString(minutes / 10);
                    String minute2 = Long.toString(minutes % 10);
                    String second1 = Long.toString(seconds / 10);
                    String second2 = Long.toString(seconds % 10);

                    mDay1.setText(day1);
                    mDay2.setText(day2);
                    mHour1.setText(hour1);
                    mHour2.setText(hour2);
                    mMinute1.setText(minute1);
                    mMinute2.setText(minute2);
                    mSecond1.setText(second1);
                    mSecond2.setText(second2);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected Activity getActivity() {
        return (Activity) getContext();
    }

    public class CountDownRunner implements Runnable{
        public void run() {
            while(!Thread.currentThread().isInterrupted() && mIsCounting){
                try {
                    doCountDown();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}
