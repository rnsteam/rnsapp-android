package com.insarennes.rnsapp.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import com.insarennes.rnsapp.R;

import java.util.ArrayList;

public class AnimatedLayout extends FrameLayout {

    protected ArrayList<View> mViews;
    protected int mCurrentIndex;

    protected Thread mSwitchThread;
    protected Boolean mIsSwitching;

    protected int mDelayTime;
    protected int mAnimationTime;
    protected int mTime;
    protected Animator mInAnimation;
    protected Animator mOutAnimation;

    public AnimatedLayout(Context context) {
        super(context);
        init();
    }

    public AnimatedLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AnimatedLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mViews = new ArrayList<>();
        mCurrentIndex = 0;
        mIsSwitching = false;
        mAnimationTime = 75;
        mTime = 4000;

        mInAnimation = ObjectAnimator.ofFloat(null, "translationY", 200, 0);
        mInAnimation.setDuration(mAnimationTime);
        mInAnimation.setStartDelay(0);
        mInAnimation.setInterpolator(new DecelerateInterpolator());
        mInAnimation.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator anim) {
                View view = (View) ((ObjectAnimator) anim).getTarget();
                view.setTranslationY(0f);
            }
        });

        mOutAnimation = ObjectAnimator.ofFloat(null, "translationY", 0, -200);
        mOutAnimation.setDuration(mAnimationTime);
        mOutAnimation.setStartDelay(0);
        mOutAnimation.setInterpolator(new AccelerateInterpolator());
        mOutAnimation.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator anim) {
                View view = (View) ((ObjectAnimator) anim).getTarget();
                view.setTranslationY(-200f);
            }
        });

        LayoutTransition layoutTransition = new LayoutTransition();
        layoutTransition.setAnimator(layoutTransition.APPEARING, mInAnimation);
        layoutTransition.setAnimator(layoutTransition.DISAPPEARING, mOutAnimation);
        setLayoutTransition(layoutTransition);
    }

    public void setAnimationTime(int animationTime) {
        this.mAnimationTime = animationTime;
    }

    public void setDelayTime(int delayTime) {
        this.mDelayTime = delayTime;
    }

    public void addAnimatedView(View view) {
        mViews.add(view);
    }

    public  void removeAllAnimatedViews() {
        stopSwitch();
        mViews.clear();
        removeAllViews();
    }

    public void startSwitch() {
        removeAllViews();
        addView(mViews.get(mCurrentIndex));
        mIsSwitching = true;
        Runnable runnable = new SwitchRunner();
        mSwitchThread = new Thread(runnable);
        mSwitchThread.start();
    }

    public void stopSwitch() {
        mIsSwitching = false;
    }

    public void doHideCurrentView() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (mViews.size() > 1) {
                    removeView(mViews.get(mCurrentIndex));
                }
            }
        });
    }

    public void doShowNextView() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (mViews.size() > 1) {
                    mCurrentIndex = (mCurrentIndex + 1) % mViews.size();
                    addView(mViews.get(mCurrentIndex));
                }
            }
        });
    }

    protected Activity getActivity() {
        return (Activity) getContext();
    }

    public class SwitchRunner implements Runnable {
        public void run() {
            try {
                Thread.sleep(mDelayTime);
                while (!Thread.currentThread().isInterrupted() && mIsSwitching) {
                    Thread.sleep(mTime);
                    doHideCurrentView();
                    Thread.sleep(mAnimationTime + 300);
                    doShowNextView();
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
