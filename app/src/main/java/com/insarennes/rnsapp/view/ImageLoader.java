package com.insarennes.rnsapp.view;

import android.app.Activity;
import android.content.res.Resources;
import android.widget.ImageView;

import com.insarennes.rnsapp.R;
import com.koushikdutta.ion.Ion;

public class ImageLoader {

    public static void loadImage(Activity activity, Resources resources, ImageView imageView, int imageResourceId, int width, int height) {
        String imageResName = resources.getResourceEntryName(imageResourceId);
        String url = "android.resource://" + activity.getPackageName() + "/drawable/" + imageResName;
        Ion.with(imageView)
                .centerCrop()
                .resize(width, height)
                .load(url);
    }

}
