package com.insarennes.rnsapp.model;

import java.util.Locale;

public class Link {

    protected LinkType type;
    protected String url;

    public Link(String url) {
        this.type = LinkType.FromUrl(url);
        this.url = url;
    }

    public Link(LinkType type, String url) {
        this.type = type;
        this.url = url;
    }

    public LinkType getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public enum LinkType {
        YOUTUBE,
        TWITTER,
        FACEBOOK,
        SOUNDCLOUD,
        DEEZER,
        SPOTIFY,
        OFFICIAL,
        EMAIL,
        FORUM;

        public static LinkType FromUrl(String url) {
            for (LinkType type : values()) {
                if (url.contains(type.toString().toLowerCase(Locale.FRANCE))) {
                    return type;
                }
            }
            if (url.contains("@")) {
                return EMAIL;
            }
            return OFFICIAL;
        }
    }

}
