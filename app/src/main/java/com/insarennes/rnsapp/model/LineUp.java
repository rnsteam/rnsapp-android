package com.insarennes.rnsapp.model;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.insarennes.rnsapp.comparator.LineUpItemComparator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LineUp {

    protected ArrayList<LineUpItem> lineUpItems;

    public LineUp() {
        lineUpItems = new ArrayList<>();
    }

    public Collection<LineUpItem> getLineUpItems() {
        return lineUpItems;
    }

    public Collection<LineUpItem> getLineUpItems(final int day) {
        Predicate<LineUpItem> isThatDay = new Predicate<LineUpItem>() {
            public boolean apply(LineUpItem item) {
                return item.getDay().get(Calendar.DAY_OF_MONTH) == day;
            }
        };
        return Lists.newArrayList((Iterables.filter(lineUpItems, isThatDay)));
    }

    public Iterable<LineUpItem> getLineUpItems(final Band band) {
        Predicate<LineUpItem> isThatBand = new Predicate<LineUpItem>() {
            public boolean apply(LineUpItem item) {
                return item.getBand().equals(band);
            }
        };
        return Iterables.filter(lineUpItems, isThatBand);
    }

    /**
     * Returns line up items categorized by stage.
     */
    public Map<Stage, Collection<LineUpItem>> getCategorizedLineUpItems(final int day) {

        // get and sort all stages :
        Iterable<LineUpItem> dayLineUpItems = getLineUpItems(day);
        Iterable<Stage> stagesIter = Iterables.transform(dayLineUpItems, getStage);
        Set<Stage> stagesSet = Sets.newHashSet(stagesIter);
        List<Stage> sortStages = new ArrayList<Stage>(stagesSet);
        Collections.sort(sortStages, new StageScoreComparator());

        // create map :
        Map<Stage, Collection<LineUpItem>> map = new LinkedHashMap<>();
        for (final Stage stage : sortStages) {
            ArrayList<LineUpItem> luiList = Lists.newArrayList((Iterables.filter(dayLineUpItems, new IsThatStagePredicate(stage))));
            map.put(stage, luiList);
        }

        return map;
    }

    public void addLineUpItem(LineUpItem lineUpItem) {
        this.lineUpItems.add(lineUpItem);
    }

    protected LineUpItem getCurrentOrNextLineUpItem(Stage stage, Boolean getNext) {
        Calendar now = RnsModel.getInstance().now();

        List<LineUpItem> lineUp = Lists.newArrayList(getCategorizedLineUpItems(LineUpItem.getDayFromDate(now).get(Calendar.DAY_OF_MONTH)).get(stage));
        Collections.sort(lineUp, new LineUpItemComparator());

        int i;
        for (i = 0 ; i < lineUp.size() - 1 ; i++) {
            LineUpItem current = lineUp.get(i);
            LineUpItem next = lineUp.get(i+1);
            if (now.after(current.getDate()) && now.before(next.getDate())) {
                if(!getNext) {
                    return current;
                }
                else {
                    return next;
                }
            }
        }
        LineUpItem current = lineUp.get(i);
        if (now.after(current.getDate())) {
            if(!getNext) {
                return current;
            }
        }
        return null;
    }

    public LineUpItem getCurrentLineUpItem(Stage stage) {
        return getCurrentOrNextLineUpItem(stage, false);
    }

    public LineUpItem getNextLineUpItem(Stage stage) {
        return getCurrentOrNextLineUpItem(stage, true);
    }

    private final class StageScoreComparator implements Comparator<Stage> {
        @Override
        public int compare(Stage stage1, Stage stage2) {
            return stage1.getScore() - stage2.getScore();
        }
    }

    private final Function<LineUpItem, Stage> getStage = new Function<LineUpItem, Stage>() {
        public Stage apply(LineUpItem lui) {
            return lui.getStage();
        }
    };

    private final class IsThatStagePredicate implements Predicate<LineUpItem> {
        private Stage stage;

        private IsThatStagePredicate(Stage stage) {
            this.stage = stage;
        }

        @Override
        public boolean apply(LineUpItem item) {
            return item.getStage().equals(stage);
        }
    }
}
