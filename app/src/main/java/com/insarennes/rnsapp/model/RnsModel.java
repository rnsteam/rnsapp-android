package com.insarennes.rnsapp.model;

import java.util.Calendar;

public class RnsModel {

    private static RnsModel mInstance;

    protected Calendar mFestivalBeginning;
    protected Calendar mFestivalEnd;

    protected Bands bands;
    protected LineUp lineUp;

    protected Stage mMainStage;
    protected Stage mSecondStage;
    protected Stage mThirdStage;

    protected Races races;

    private RnsModel() {
        mFestivalBeginning = Calendar.getInstance();
        mFestivalBeginning.set(Calendar.DAY_OF_MONTH, 13);
        mFestivalBeginning.set(Calendar.MONTH, Calendar.MAY);
        mFestivalBeginning.set(Calendar.YEAR, 2015);
        mFestivalBeginning.set(Calendar.HOUR_OF_DAY, 21);
        mFestivalBeginning.set(Calendar.MINUTE, 00);

        mFestivalEnd = Calendar.getInstance();
        mFestivalEnd.set(Calendar.DAY_OF_MONTH, 17);
        mFestivalEnd.set(Calendar.MONTH, Calendar.MAY);
        mFestivalEnd.set(Calendar.YEAR, 2015);
        mFestivalEnd.set(Calendar.HOUR_OF_DAY, 18);
        mFestivalEnd.set(Calendar.MINUTE, 00);

        bands = new Bands();
        lineUp = new LineUp();
        races = new Races();
    }

    public static RnsModel getInstance() {
        if (mInstance == null)
            mInstance = new RnsModel();
        return mInstance;
    }

    public Calendar now() {
        return Calendar.getInstance();
    }

    public Calendar getFestivalBeginning() {
        return mFestivalBeginning;
    }

    public Calendar getFestivalEnd() {
        return mFestivalEnd;
    }

    public Bands getBands() {
        return bands;
    }

    public LineUp getLineUp() {
        return lineUp;
    }

    public Races getRaces() {
        return races;
    }

    public Stage getMainStage() {
        return mMainStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mMainStage = mainStage;
    }

    public Stage getSecondStage() {
        return mSecondStage;
    }

    public void setSecondStage(Stage secondStage) {
        this.mSecondStage = secondStage;
    }

    public Stage getThirdStage() {
        return mThirdStage;
    }

    public void setThirdStage(Stage thirdStage) {
        this.mThirdStage = thirdStage;
    }
}
