package com.insarennes.rnsapp.model;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.insarennes.rnsapp.comparator.LineUpItemComparator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Races {

    protected ArrayList<Race> races;

    public Races() {
        races = new ArrayList<>();
    }

    public List<Race> getRaces() {
        return races;
    }

    public void addRace(Race race) {
        this.races.add(race);
    }

    public Race getRaceFromId(int id) {
        for (Race g : getRaces()) {
            if (g.getId() == id) {
                return g;
            }
        }
        return null;
    }

    public Race getCurrentRace() {
        Calendar now = RnsModel.getInstance().now();

        Collections.sort(races, new Comparator<Race>() {
            @Override
            public int compare(Race race1, Race race2) {
                return race1.getDate().compareTo(race2.getDate());
            }
        });

        for (Race race : races) {
            if (now.get(Calendar.DAY_OF_MONTH) == race.getDate().get(Calendar.DAY_OF_MONTH)&& now.get(Calendar.HOUR_OF_DAY) > 12 && now.get(Calendar.HOUR_OF_DAY) < 18) {
                return race;
            }
        }
        return null;
    }
}
