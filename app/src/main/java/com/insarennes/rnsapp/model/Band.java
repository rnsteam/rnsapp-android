package com.insarennes.rnsapp.model;

import java.util.ArrayList;

public class Band {

    protected int id;
    protected String name;
    protected String description;
    protected String genre;
    protected String country;
    protected int headliningScrore;

    protected ArrayList<Artist> artists;
    protected ArrayList<Link> links;

    protected int imageResId;

    public Band(int id, String name, String description, String genre, String country) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.genre = genre;
        this.country = country;
        this.links = new ArrayList<Link>();
    }

    public Band(int id, String name, String description, String genre, String country, int imageResId) {
        this(id, name, description, genre, country);
        this.imageResId = imageResId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public int getHeadliningScrore() {
        return headliningScrore;
    }

    public void setHeadliningScrore(int headliningScrore) {
        this.headliningScrore = headliningScrore;
    }

    public Iterable<Artist> getArtists() {
        return artists;
    }

    public void addArtist(Artist artist) {
        this.artists.add(artist);
    }

    public Iterable<Link> getLinks() {
        return this.links;
    }

    public void addLink(String link) {
        this.links.add(new Link(link));
    }

}
