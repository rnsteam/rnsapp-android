package com.insarennes.rnsapp.fonts;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Give access to all custom fonts everywhere in the application.
 * The font are loaded once.
 * (code founded in StackOverflow)
 */
public class CustomFontsLoader {

    public static final int ARCA_HEAVY = 0;
    public static final int BOOMTOWN_DECO = 1;

    private static final int NUM_OF_CUSTOM_FONTS = 2;

    private static Typeface[] fonts = new Typeface[NUM_OF_CUSTOM_FONTS];

    private static String[] fontPath = {
            "fonts/Arca-Heavy.ttf",
            "fonts/BoomtownDeco.otf"
    };

    private static boolean fontsLoaded = false;

    /**
     * Returns a loaded custom font based on it's identifier.
     *
     * @param context        - the current context
     * @param fontIdentifier = the identifier of the requested font
     * @return Typeface object of the requested font.
     */
    public static Typeface getTypeface(Context context, int fontIdentifier) {
        if (!fontsLoaded) {
            loadFonts(context);
        }
        return fonts[fontIdentifier];
    }

    private static void loadFonts(Context context) {
        for (int i = 0; i < NUM_OF_CUSTOM_FONTS; i++) {
            fonts[i] = Typeface.createFromAsset(context.getAssets(), fontPath[i]);
        }
        fontsLoaded = true;

    }
}
