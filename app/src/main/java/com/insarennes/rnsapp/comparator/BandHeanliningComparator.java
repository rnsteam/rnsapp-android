package com.insarennes.rnsapp.comparator;

import com.insarennes.rnsapp.model.Band;

import java.util.Comparator;

public final class BandHeanliningComparator implements Comparator<Band> {

    @Override
    public final int compare(Band band1, Band band2) {
        return band1.getHeadliningScrore() - band2.getHeadliningScrore();
    }
}