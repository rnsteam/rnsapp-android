package com.insarennes.rnsapp.comparator;

import com.insarennes.rnsapp.model.Band;

import java.util.Comparator;

public class BandNameComparator implements Comparator<Band> {

    @Override
    public int compare(Band band1, Band band2) {
        return band1.getName().compareTo(band2.getName());
    }
}
