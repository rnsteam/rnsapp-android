package com.insarennes.rnsapp.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;

public class DrawerAdapter extends ArrayAdapter<String> {

    public DrawerAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.adapter_drawer_item, parent, false);

        Typeface boomtownDecoFont = CustomFontsLoader.getTypeface(getContext(), CustomFontsLoader.BOOMTOWN_DECO);

        TextView label = (TextView) view.findViewById(R.id.drawer_item_label);
        label.setTypeface(boomtownDecoFont);
        label.setText(getItem(position));

        return view;
    }
}
