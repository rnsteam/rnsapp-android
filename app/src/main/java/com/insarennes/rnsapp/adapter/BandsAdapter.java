package com.insarennes.rnsapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.activity.BandActivity;
import com.insarennes.rnsapp.comparator.BandDateComparator;
import com.insarennes.rnsapp.comparator.BandHeanliningComparator;
import com.insarennes.rnsapp.comparator.BandNameComparator;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;
import com.insarennes.rnsapp.model.Band;
import com.insarennes.rnsapp.model.Bands;
import com.insarennes.rnsapp.model.LineUp;
import com.insarennes.rnsapp.model.LineUpItem;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class BandsAdapter extends RecyclerView.Adapter<BandsAdapter.ViewHolder> {

    private Bands mBands;
    private LineUp mLineUp;
    private Activity mActivity;
    private Bitmap mPlaceHolderBitmap;

    protected ArrayList<Band> mSortedBands;

    public BandsAdapter(Activity activity, Bands bands, LineUp lineUp) {
        mPlaceHolderBitmap = null; // No place holder bitmap set for waiting

        this.mActivity = activity;
        this.mBands = bands;
        this.mLineUp = lineUp;

        this.mSortedBands = Lists.newArrayList(mBands.getBands());
        Collections.sort(mSortedBands, new BandHeanliningComparator());
    }

    public void setSort(BandSort sort) {
        switch (sort) {
            case HEADLININGS:
                Collections.sort(mSortedBands, new BandHeanliningComparator());
                break;
            case NAME:
                Collections.sort(mSortedBands, new BandNameComparator());
                break;
            case DATE:
                Collections.sort(mSortedBands, new BandDateComparator(mLineUp));
                break;
        }
        notifyDataSetChanged();
    }

    protected Band getBand(int position) {
        return mSortedBands.get(position);
    }

    public static boolean cancelPotentialWork(int data, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final int bitmapData = bitmapWorkerTask.data;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData == 0 || bitmapData != data) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mSortedBands == null ? 0 : mSortedBands.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        final Context context = parent.getContext();

        View v = LayoutInflater.from(context).inflate(R.layout.adapter_band, parent, false);
        return new ViewHolder(v, new IItemClicked() {
            @Override
            public void onItemClicked(View v, int position) {
                String transitionImage = context.getString(R.string.transition_album_cover);
//                String transitionName = context.getString(R.string.transition_band_name);
//                String transitionGenre = context.getString(R.string.transition_band_genre);
//                String transitionDay = context.getString(R.string.transition_band_day);
                ImageView image = (ImageView) v.findViewById(R.id.card_image);
//                TextView name = (TextView) v.findViewById(R.id.band_name);
//                TextView genre = (TextView) v.findViewById(R.id.band_genre);
//                TextView day = (TextView) v.findViewById(R.id.band_day);

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity,
                        image, transitionImage);

//                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity,
//                        new Pair<View, String>(image, transitionImage)
//                        new Pair<View, String>(name, transitionName),
//                        new Pair<View, String>(genre, transitionGenre),
//                        new Pair<View, String>(day, transitionDay)
//                );
                Intent i = new Intent(context, BandActivity.class);
                i.putExtra(BandActivity.BAND_ID_KEY, getBand(position).getId());
                ActivityCompat.startActivityForResult(mActivity, i, BandActivity.OPEN_LINEUP_REQUESTCODE, options.toBundle());
            }
        });
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Band group = getBand(position);
        LineUpItem lineUp = Iterables.getFirst(mLineUp.getLineUpItems(group), null);
        holder.name.setText(group.getName());
        holder.genre.setText(group.getGenre());

        if (lineUp != null) {
            holder.day.setText(getDay(lineUp.getDay()));
        }

        holder.card.setOnClickListener(holder);
        loadArtistCard(group.getImageResId(), holder);

        final int TRANSPARENT_GRAY = 0x88444444;
        holder.setRectangleColor(Color.WHITE, TRANSPARENT_GRAY); // color will be changed depending on new image
    }

    public void loadArtistCard(int imageResId, ViewHolder holder) {
        if (cancelPotentialWork(imageResId, holder.image)) {
            final BitmapWorkerTask task = new BitmapWorkerTask(holder);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(mActivity.getResources(), mPlaceHolderBitmap, task);
            holder.image.setImageDrawable(asyncDrawable);
            task.execute(imageResId);
        }
    }

    protected String getDay(Calendar day) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        return sdf.format(day.getTime()).toUpperCase().substring(0,1) + day.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Holder pour la structure de la View
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name;
        TextView genre;
        TextView day;
        ImageView image;
        CardView card;
        RelativeLayout textLayout;
        IItemClicked mListener;

        public ViewHolder(View itemView, IItemClicked listener) {
            super(itemView);
            card = (CardView) itemView.findViewById(R.id.card_view);
            textLayout = (RelativeLayout) itemView.findViewById(R.id.cardview_layout_text);
            image = (ImageView) itemView.findViewById(R.id.card_image);
            name = (TextView) itemView.findViewById(R.id.band_name);
            genre = (TextView) itemView.findViewById(R.id.band_genre);
            day = (TextView) itemView.findViewById(R.id.band_day);

            Typeface arcaHeavyFont = CustomFontsLoader.getTypeface(mActivity, CustomFontsLoader.ARCA_HEAVY);
            name.setTypeface(arcaHeavyFont);
            genre.setTypeface(arcaHeavyFont);
            day.setTypeface(arcaHeavyFont);

            card.setOnClickListener(this);
            mListener = listener;
        }

        @Override
        public void onClick(View view) {
            if (mListener != null)
                mListener.onItemClicked(view, getPosition());
        }

        private void setRectangleColor(int textColor, int background) {
            textLayout.setBackgroundColor(background);
            name.setTextColor(textColor);
            genre.setTextColor(textColor);
            day.setTextColor(textColor);
        }
    }

    public interface IItemClicked {
        public void onItemClicked(View v, int position);
    }

    static class AsyncDrawable extends BitmapDrawable {

        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference = new WeakReference<>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }

    class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {

        private final WeakReference<ViewHolder> holderViewReference;
        private int data = 0;

        public BitmapWorkerTask(ViewHolder holder) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            holderViewReference = new WeakReference<>(holder);
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(Integer... params) {
            data = params[0];
            Bitmap image = BitmapFactory.decodeResource(mActivity.getResources(), data);
            return ThumbnailUtils.extractThumbnail(image, 200, 200);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            if (holderViewReference != null && bitmap != null) {
                final ImageView imageView = holderViewReference.get().image;
                final BitmapWorkerTask bitmapWorkerTask =
                        getBitmapWorkerTask(imageView);
                if (this == bitmapWorkerTask && imageView != null) {
                    imageView.setImageBitmap(bitmap);
                    setColorRectangle(holderViewReference.get(), bitmap);
                }
            }
        }

        private void setColorRectangle(final ViewHolder holder, Bitmap bitmap) {
            Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    Palette.Swatch vibrant = palette.getVibrantSwatch();
                    if (vibrant != null) {
                        int bgColor = vibrant.getRgb();
                        int bgTransparent = Color.argb(203, Color.red(bgColor), Color.green(bgColor), Color.blue(bgColor));
                        //int textColor = vibrant.getBodyTextColor();
                        int textColor = Color.WHITE;
                        holder.setRectangleColor(textColor, bgTransparent);
                    }
                }
            });
        }
    }

    public enum BandSort {
        NAME,
        DATE,
        HEADLININGS
    }
}
